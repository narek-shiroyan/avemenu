import {
    CLEAR_CART,
    DELETE_CART_ITEM,
    SET_CART,
} from '../constants/actionTypes';

export const _setCart = food => async dispatch => {
    // console.log('FOOD CART', food);
    await dispatch({ type: SET_CART, payload: food });
};

export const _deleteCartItem = food => async dispatch => {
    await dispatch({ type: DELETE_CART_ITEM, payload: food });
};

export const _clearCart = () => async dispatch => {
    // console.log('CLEAR CART');
    await dispatch({ type: CLEAR_CART });
};
