import {
  CLEAR_ORDER,
  DELETE_ORDER_ITEM,
  SET_ORDER,
} from '../constants/actionTypes';

export const _setOrder = food => async dispatch => {
  // console.log('FOOD', food);
  await dispatch({ type: SET_ORDER, payload: food });
};

export const _deleteOrderItem = food => async dispatch => {
  await dispatch({ type: DELETE_ORDER_ITEM, payload: food });
};

export const _clearOrdersItem = () => async dispatch => {
  await dispatch({ type: CLEAR_ORDER });
};
