import { CLEAR_CAFE_INFO, SET_CAFE_INFO } from '../constants/actionTypes';

export const _setCafeInfo =
  (table, cafe, url, tableLocation) => async dispatch => {
    await dispatch({
      type: SET_CAFE_INFO,
      payload: { table, cafe, url, tableLocation },
    });
  };

export const _clearCafeInfo = () => async dispatch => {
  await dispatch({ type: CLEAR_CAFE_INFO });
};
