import { Heading, HStack, Spinner } from 'native-base';
import React from 'react';

export default function Loading({ title }) {
  return (
    <HStack space={2} alignItems="center">
      <Spinner accessibilityLabel="Loading posts" />
      <Heading color="primary.500" fontSize="md">
        {title}
      </Heading>
    </HStack>
  );
}
