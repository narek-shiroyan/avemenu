import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { FONTS } from '../../constants/fonts';

export default function PressButton({ title, cb }) {
  return (
    <TouchableOpacity style={styles.container} onPress={cb}>
      <Text style={styles.text__button}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: 'tomato',
    height: 30,
    borderRadius: 5,
    paddingHorizontal: 15,
    margin: 10,
  },
  text__button: {
    alignSelf: 'center',
    ...FONTS.btnCaption.white15px,
  },
});
