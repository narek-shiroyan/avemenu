import React from 'react';
import {
  ActivityIndicator,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { COLORS } from '../../constants/colors';
import { FONTS } from '../../constants/fonts';
import { getImage } from '../../utils/images';

export default function CurrentCafe(props) {
  const { cafe, cb } = props;

  if (!cafe) {
    return <ActivityIndicator />;
  }

  // console.log('malvina', getImage(cafe.images[0], 300, 300));
  return (
    <View style={styles.container}>
      <View style={styles.view__container}>
        <Image
          source={{
            uri: getImage(cafe.images[0], 300, 300),
          }}
          style={styles.cafeImage}
        />

        <View style={styles.text__container}>
          <Text style={styles.text__sub}>Текущий заказ</Text>
          <Text style={styles.text__title}>{cafe.name}</Text>
          <TouchableOpacity onPress={cb} style={styles.button}>
            <Text style={styles.button__text}>ПЕРЕЙТИ</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '95%',
    // height: 50,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    position: 'absolute',
    bottom: 20,
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,

    elevation: 7,
  },
  view__container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  image__container: {
    // width: '30%',
  },
  button: {
    backgroundColor: COLORS.coral,
    alignSelf: 'center',
    justifyContent: 'center',
    width: '100%',
    borderRadius: 10,
    marginTop: 20,
  },
  button__text: {
    alignSelf: 'center',
    padding: 5,
    ...FONTS.btnCaption.white15px,
  },
  text__container: {
    // backgroundColor: '#f2f',
    width: '50%',
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
  },
  text__sub: {
    ...FONTS.p_14px_text,
    textAlign: 'center',
  },
  text__title: {
    ...FONTS.btnCaption.h4_16px_dark,
    textAlign: 'center',
    marginTop: 5,
  },
  cafeImage: {
    height: 100,
    width: 100,
    alignSelf: 'center',
    borderRadius: 8,
    margin: 15,
  },
});
