import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { COLORS } from '../../constants/colors';
import { CURRENCY } from '../../constants/currency';
import { FONTS } from '../../constants/fonts';
import { getImage } from '../../utils/images';

export default function FoodCart(props) {
  const { images, name, price, portion, click } = props;
  return (
    <TouchableOpacity onPress={click} style={styles.container}>
      <View style={styles.image__container}>
        {images.length ? (
          <Image
            source={{
              uri: images.length ? getImage(images[0], null, 200) : null,
            }}
            resizeMethod="auto"
            resizeMode="cover"
            style={styles.image}
          />
        ) : (
          <View style={styles.defaultImage} />
        )}
        <View style={styles.price__container}>
          <Text style={styles.price__text}>
            {price}
            {CURRENCY.ru}
          </Text>
        </View>
      </View>
      <View style={styles.text__container}>
        <Text style={styles.title__text}>{name}</Text>
        <Text style={styles.portion__text}>{portion}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 250,
    alignItems: 'center',
    //backgroundColor: '#f2f',
  },
  image__container: {
    width: '100%',
    height: 180,
    borderRadius: 16,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 16,
  },
  defaultImage: {
    width: '100%',
    height: 180,
    borderRadius: 16,
    backgroundColor: COLORS.lightPurple2,
  },
  price__container: {
    width: 50,
    height: 30,
    backgroundColor: COLORS.coral,
    position: 'absolute',
    bottom: 0,
    borderBottomLeftRadius: 16,
    borderTopRightRadius: 16,
    justifyContent: 'center',
  },
  price__text: {
    ...FONTS.secondary_14px_price_old,
    alignSelf: 'center',
  },
  title__text: {
    ...FONTS.btnCaption.h4_16px_dark,
  },
  portion__text: {
    ...FONTS.secondary_13px_text_passive,
  },
  text__container: {
    height: 60,
    width: '98%',
    marginTop: 10,
  },
});
