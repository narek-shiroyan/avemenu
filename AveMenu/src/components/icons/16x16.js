import React from 'react';
import Breakfast from '../../assets/icons/16x16/breakfast.svg';
import Cancelled from '../../assets/icons/16x16/cancelled.svg';
import Dessert from '../../assets/icons/16x16/dessert.svg';
import Done from '../../assets/icons/16x16/done.svg';
import Down from '../../assets/icons/16x16/down.svg';
import Drink from '../../assets/icons/16x16/drink.svg';
import FastFood from '../../assets/icons/16x16/fastfood.svg';
import Fish from '../../assets/icons/16x16/fish.svg';
import FullStar from '../../assets/icons/16x16/full_star.svg';
import HalfStar from '../../assets/icons/16x16/half_star.svg';
import HotMeals from '../../assets/icons/16x16/hot_meals.svg';
import MainCourse from '../../assets/icons/16x16/main_course.svg';
import Meat from '../../assets/icons/16x16/meat.svg';
import OutlineStar from '../../assets/icons/16x16/outline_star.svg';
import Pizza from '../../assets/icons/16x16/pizza.svg';
import Salad from '../../assets/icons/16x16/salad.svg';
import Truck from '../../assets/icons/16x16/truck.svg';
import Wallet from '../../assets/icons/16x16/wallet.svg';

import WarnSalad from '../../assets/icons/16x16/warmsalad.svg';
import Garnik from '../../assets/icons/16x16/garnik.svg';
import Snacks from '../../assets/icons/16x16/snacks.svg';
import Sause from '../../assets/icons/16x16/sauce.svg';
import Bacery from '../../assets/icons/16x16/desert.svg';
import Drinks from '../../assets/icons/16x16/drinks.svg';

const flags = {
    Breakfast: Breakfast,
    Cancelled: Cancelled,
    Dessert: Dessert,
    Done: Done,
    Down: Down,
    Drink: Drink,
    FastFood: FastFood,
    Fish: Fish,
    FullStar: FullStar,
    HalfStar: HalfStar,
    HotMeals: HotMeals,
    MainCourse: MainCourse,
    Meat: Meat,
    OutlineStar: OutlineStar,
    Pizza: Pizza,
    Salad: Salad,
    Truck: Truck,
    Wallet: Wallet,
    WarnSalad: WarnSalad,
    Garnik: Garnik,
    Snacks: Snacks,
    Sause: Sause,
    Bacery: Bacery,
    Drinks: Drinks,
};

export default function FoodIconCustom(props) {
    const IconFlag = flags[props.name];
    const size = props.size;
    return (
        <IconFlag
            width={size}
            height={size}
            style={props.style ? props.style : null}
        />
    );
}