import React from 'react';
import Breakfast from '../../assets/icons/16x16_white/breakfast.svg';
import Dessert from '../../assets/icons/16x16_white/dessert.svg';
import Drink from '../../assets/icons/16x16_white/drink.svg';
import FastFood from '../../assets/icons/16x16_white/fastfood.svg';
import Fish from '../../assets/icons/16x16_white/fish.svg';
import FullStar from '../../assets/icons/16x16_white/full_star.svg';
import HalfStar from '../../assets/icons/16x16_white/half_star.svg';
import HotMeals from '../../assets/icons/16x16_white/hot_meals.svg';
import MainCourse from '../../assets/icons/16x16_white/main_course.svg';
import Meat from '../../assets/icons/16x16_white/meat.svg';
import Pizza from '../../assets/icons/16x16_white/pizza.svg';
import Salad from '../../assets/icons/16x16_white/salad.svg';

import WarnSalad from '../../assets/icons/16x16_white/warmsalad.svg';
import Garnik from '../../assets/icons/16x16_white/garnik.svg';
import Snacks from '../../assets/icons/16x16_white/snacks.svg';
import Sause from '../../assets/icons/16x16_white/sauce.svg';
import Bacery from '../../assets/icons/16x16_white/desert.svg';
import Drinks from '../../assets/icons/16x16_white/drinks.svg';

const flags = {
    Breakfast: Breakfast,
    Dessert: Dessert,
    Drink: Drink,
    FastFood: FastFood,
    Fish: Fish,
    FullStar: FullStar,
    HalfStar: HalfStar,
    HotMeals: HotMeals,
    MainCourse: MainCourse,
    Meat: Meat,
    Pizza: Pizza,
    Salad: Salad,
    WarnSalad: WarnSalad,
    Garnik: Garnik,
    Snacks: Snacks,
    Sause: Sause,
    Bacery: Bacery,
    Drinks: Drinks,
};

export default function FoodIconCustomWhite(props) {
    const IconFlag = flags[props.name];
    const size = props.size;
    return (
        <IconFlag
            width={size}
            height={size}
            style={props.style ? props.style : null}
        />
    );
}