import React from 'react';
import Clock from '../../assets/icons/20x20_coral/clock.svg';
import Pin from '../../assets/icons/20x20_coral/pin.svg';

const flags = {
    Clock: Clock,
    Pin: Pin,
};

export default function IconCustom(props) {
    const IconFlag = flags[props.name];
    const size = props.size;
    return (
        <IconFlag
            width={size}
            height={size}
            style={props.style ? props.style : null}
        />
    );
}
