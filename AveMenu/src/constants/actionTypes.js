// Заказы
export const SET_ORDER = 'SET_ORDER';
export const DELETE_ORDER_ITEM = 'DELETE_ORDER_ITEM';
export const CLEAR_ORDER = 'CLEAR_ORDER';

// Корзина
export const SET_CART = 'SET_CART';
export const DELETE_CART_ITEM = 'DELETE_CART_ITEM';
export const CLEAR_CART = 'CLEAR_CART';

export const SET_CAFE_INFO = 'SET_CAFE_INFO';
export const CLEAR_CAFE_INFO = 'CLEAR_CAFE_INFO';
