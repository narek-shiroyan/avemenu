export const MAIN_SCREEN = 'Главная';

export const PROFILE_SCREEN = 'Профиль';
export const ABOUT_SCREEN = 'О приложении';
export const POLICY_SCREEN = 'Политика';
export const TERMS_SCREEN = 'Условия';

export const CAMERA_SCREEN = 'Камера';
export const MENU_SCREEN = 'Меню';
export const FOOD_SCREEN = 'Блюдо';
export const CART_SCREEN = 'Корзина';
export const ORDERS_SCREEN = 'Заказы';
