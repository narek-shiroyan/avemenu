import React from 'react';
import { StatusBar, StyleSheet, View } from 'react-native';
import { COLORS } from '../constants/colors';

export default function Container(props) {
  return (
    <View style={[styles.container, props.customStyles]}>
      <StatusBar barStyle="light-content" />
      {props.children}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: COLORS.white,
  },
});
