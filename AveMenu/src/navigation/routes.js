import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { View, Platform } from 'react-native';
import { connect } from 'react-redux';
import { _clearCafeInfo, _setCafeInfo } from '../actions/user';
import IconButton from '../components/buttons/IconButton';
import { COLORS } from '../constants/colors';
import {
  ABOUT_SCREEN,
  CAMERA_SCREEN,
  CART_SCREEN,
  FOOD_SCREEN,
  MAIN_SCREEN,
  MENU_SCREEN,
  ORDERS_SCREEN,
  POLICY_SCREEN,
  PROFILE_SCREEN,
  TERMS_SCREEN,
} from '../constants/navigations';
import CameraScreen from '../screens/Camera';
import CartScreen from '../screens/Cart';
import FoodScreen from '../screens/Food';
import MainMapScreen from '../screens/MainMap';
import MenuScreen from '../screens/Menu';
import OrdersScreen from '../screens/Orders';
import ProfileScreen from '../screens/Profile';
import { getCountOrders } from '../utils/scripts';
import AboutScreen from '../screens/About';
import PolicyScreen from '../screens/Policy';
import TermsScreen from '../screens/Terms';

const MainStack = createStackNavigator();

const MainNavigator = props => {
  // useEffect(() => {
  //     loadStorage();
  // }, []);

  return (
    <MainStack.Navigator initialRouteName={MAIN_SCREEN}>
      <MainStack.Screen
        name={MAIN_SCREEN}
        options={{ headerShown: false }}
        component={MainMapScreen}
      />

      <MainStack.Group screenOptions={{ presentation: 'modal' }}>
        <MainStack.Screen name={PROFILE_SCREEN} component={ProfileScreen} />
        <MainStack.Screen name={ABOUT_SCREEN} component={AboutScreen} />
        <MainStack.Screen name={POLICY_SCREEN} component={PolicyScreen} />
        <MainStack.Screen name={TERMS_SCREEN} component={TermsScreen} />
      </MainStack.Group>

      <MainStack.Screen
        name={MENU_SCREEN}
        options={({ navigation }) => ({
          title: 'Меню',
          headerStyle: {
            shadowColor: 'transparent',
          },
          headerLeft: () => (
            <IconButton
              name="ArrowLeft"
              customStyles={{
                backgroundColor: COLORS.lightPurple1,
                marginLeft: 10,
              }}
              cb={async () => {
                navigation.goBack();
                props._clearCafeInfo();
              }}
            />
          ),
          headerRight: () => (
            <View style={{ flexDirection: 'row' }}>
              <IconButton
                name="Orders"
                count={props.orders.length ? props.orders.length : false}
                customStyles={{
                  backgroundColor: COLORS.lightPurple1,
                  marginRight: 10,
                }}
                cb={() => navigation.navigate(ORDERS_SCREEN)}
              />
              <IconButton
                name="CartOutline"
                count={
                  Object.keys(props.cart).length
                    ? getCountOrders(props.cart)
                    : false
                }
                cb={() => navigation.navigate(CART_SCREEN)}
                customStyles={{
                  backgroundColor: COLORS.lightPurple1,
                  marginRight: 10,
                }}
              />
            </View>
          ),
        })}
        component={MenuScreen}
      />
      <MainStack.Group screenOptions={{ presentation: 'modal' }}>
        <MainStack.Screen
          // options={{ headerShown: false }}
          options={({ navigation }) => ({
            //headerShown: false,
            headerTitle: '',
            headerTransparent: Platform.OS === 'android' ? false : true,
            headerLeft: null,
            headerRight: () => (
              <IconButton
                name="Close"
                customStyles={{
                  backgroundColor: COLORS.lightPurple1,
                  marginRight: 10,
                  zIndex: 10,
                }}
                cb={async () => {
                  navigation.goBack();
                }}
              />
            ),
          })}
          name={FOOD_SCREEN}
          component={FoodScreen}
        />
      </MainStack.Group>
      <MainStack.Group screenOptions={{ presentation: 'modal' }}>
        <MainStack.Screen
          options={({ navigation }) => ({
            //headerShown: false,
            headerTitle: '',
            headerTransparent: Platform.OS === 'android' ? false : true,
            headerLeft: null,
            headerRight: () => (
              <IconButton
                name="Close"
                customStyles={{
                  backgroundColor: COLORS.lightPurple1,
                  marginRight: 10,
                  zIndex: 10,
                }}
                cb={async () => {
                  navigation.goBack();
                }}
              />
            ),
          })}
          name={CART_SCREEN}
          component={CartScreen}
        />
      </MainStack.Group>
      <MainStack.Group screenOptions={{ presentation: 'modal' }}>
        <MainStack.Screen
          options={({ navigation }) => ({
            //headerShown: false,
            headerTitle: '',
            headerTransparent: Platform.OS === 'android' ? false : true,
            headerLeft: null,
            headerRight: () => (
              <IconButton
                name="Close"
                customStyles={{
                  backgroundColor: COLORS.lightPurple1,
                  marginRight: 10,
                  zIndex: 10,
                }}
                cb={async () => {
                  navigation.goBack();
                }}
              />
            ),
          })}
          name={ORDERS_SCREEN}
          component={OrdersScreen}
        />
      </MainStack.Group>
      <MainStack.Group screenOptions={{ presentation: 'modal' }}>
        <MainStack.Screen
          name={CAMERA_SCREEN}
          component={CameraScreen}
          options={({ navigation }) => ({
            //headerShown: false,
            headerTitle: '',
            //headerTransparent: true,
            // headerLeft: null,
            // headerRight: () => (
            //     <IconButton
            //         name="Close"
            //         customStyles={{
            //             backgroundColor: COLORS.lightPurple1,
            //             marginRight: 10,
            //             zIndex: 30,
            //         }}
            //         cb={async () => {
            //             navigation.goBack();
            //         }}
            //     />
            // ),
          })}
        />
      </MainStack.Group>
    </MainStack.Navigator>
  );
};

const mapStateToProps = state => ({
  cart: state.cart.items,
  orders: state.orders.items,
});

const mapDispatchToProps = {
  _clearCafeInfo,
  _setCafeInfo,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainNavigator);
