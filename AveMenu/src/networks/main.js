import axios from 'axios';
import { HOST, TOKEN } from '../constants/config';

export const getAllCafes = () => {
  return axios({
    method: 'GET',
    url: `${HOST}/cafe`,
    headers: {
      'Access-Token': TOKEN,
    },
  });
};

export const sendQRcode = qr => {
  return axios({
    method: 'GET',
    url: `${qr}`,
    headers: {
      'Access-Token': TOKEN,
    },
  });
};

export const sendQR = qr => {
  return axios({
    method: 'POST',
    url: `${HOST}/qr`,
    headers: {
      'Access-Token': TOKEN,
    },
    data: {
      qr,
    },
  });
};

export const createOrder = (
  items,
  total,
  table,
  cafeId,
  date,
  tableLocation,
) => {
  return axios({
    method: 'POST',
    url: `${HOST}/order/create`,
    headers: {
      'Access-Token': TOKEN,
    },
    data: {
      items,
      total,
      table,
      cafeId,
      date,
      tableLocation,
    },
  });
};

export const getOrderStatus = orderId => {
  return axios({
    method: 'GET',
    url: `${HOST}/order/status/${orderId}`,
    headers: {
      'Access-Token': TOKEN,
    },
  });
};

export const changeOrderStatus = (orderId, status) => {
  return axios({
    method: 'PUT',
    url: `${HOST}/order/status/update`,
    headers: {
      'Access-Token': TOKEN,
    },
    data: {
      orderId,
      status,
    },
  });
};

export const getOrdersByArray = orders => {
  return axios({
    method: 'POST',
    url: `${HOST}/orders/array`,
    headers: {
      'Access-Token': TOKEN,
    },
    data: {
      orders,
    },
  });
};
