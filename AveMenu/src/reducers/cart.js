import {
  CLEAR_CART,
  DELETE_CART_ITEM,
  SET_CART,
} from '../constants/actionTypes';

const initialState = {
  items: {},
};

/**
 * _id: {
 * name: 'asdasd'
 * count: 2
 * }
 */
export default function cart(state = initialState, action) {
  switch (action.type) {
    case SET_CART: {
      // console.log('REDUCER ITEM', action.payload);

      return {
        ...state,
        items: {
          ...state.items,
          [action.payload._id]: action.payload,
        },
      };
    }
    case DELETE_CART_ITEM: {
      delete state.items[action.payload._id];
      let newRemovedItems = state.items;
      return {
        ...state,
        items: {
          ...newRemovedItems,
        },
      };
    }
    case CLEAR_CART: {
      return {
        ...state,
        items: {},
      };
    }
    default:
      return state;
  }
}
