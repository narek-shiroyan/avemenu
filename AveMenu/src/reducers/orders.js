import {
  DELETE_ORDER_ITEM,
  SET_ORDER,
  CLEAR_ORDER,
} from '../constants/actionTypes';

const initialState = {
  items: [],
};

/**
 * _id: {
 * name: 'asdasd'
 * count: 2
 * }
 */
export default function order(state = initialState, action) {
  switch (action.type) {
    case SET_ORDER: {
      return {
        ...state,
        items: action.payload,
      };
    }
    case DELETE_ORDER_ITEM: {
      delete state.items[action.payload._id];
      let newRemovedItems = state.items;
      return {
        ...state,
        items: {
          ...newRemovedItems,
        },
      };
    }
    case CLEAR_ORDER: {
      return {
        ...state,
        items: [],
      };
    }
    default:
      return state;
  }
}
