import { CLEAR_CAFE_INFO, SET_CAFE_INFO } from '../constants/actionTypes';

const initialState = {
  table: null,
  cafe: {},
  url: null,
  tableLocation: null,
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case SET_CAFE_INFO: {
      return {
        ...state,
        table: action.payload.table,
        cafe: action.payload.cafe,
        url: action.payload.url,
        tableLocation: action.payload.tableLocation,
      };
    }
    case CLEAR_CAFE_INFO: {
      return {
        ...state,
        table: null,
        cafe: {},
        url: null,
        tableLocation: null,
      };
    }
    default:
      return state;
  }
}
