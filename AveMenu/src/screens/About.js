import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { FONTS } from '../constants/fonts';
import { DEVICE_HEIGHT } from '../constants/size';
import Container from '../layouts/Container';
import { AboutAppText } from '../utils/text';

export default function AboutScreen() {
  return (
    <Container>
      <View style={styles.container}>
        <Image
          source={require('../assets/Illustration.png')}
          style={styles.image}
          resizeMethod={'auto'}
          resizeMode={'contain'}
        />
        <Text style={styles.text__info}>{AboutAppText}</Text>
        <Text style={styles.text__info}>Версия приложения 1.4</Text>
      </View>
    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
  image: {
    //width: '100%',
    height: DEVICE_HEIGHT / 3.5,
    alignSelf: 'center',
    marginBottom: 50,
  },
  text__info: {
    ...FONTS.p_16px_dark_center,
    marginTop: 10,
  },
});
