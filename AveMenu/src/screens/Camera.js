import React, { useState } from 'react';
import { Alert, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { connect } from 'react-redux';
import { _setCafeInfo } from '../actions/user';
import QRScanner from '../assets/icons/scaner.svg';
import TopWhite from '../components/panel/TopWhite';
import { FONTS } from '../constants/fonts';
import { MENU_SCREEN } from '../constants/navigations';
import Container from '../layouts/Container';
import { sendQRcode } from '../networks/main';
import { checkUrl } from '../utils/regex';

function CameraScreen(props) {
  const [scanned, setScanned] = useState(false);

  const camera = React.useRef(null);
  return (
    <Container>
      <RNCamera
        ref={camera}
        style={styles.preview}
        type={RNCamera.Constants.Type.back}
        flashMode={RNCamera.Constants.FlashMode.on}
        notAuthorizedView={renderEmpty()}
        onBarCodeRead={url => {
          if (scanned) {
            return;
          }
          // console.log(url.data);
          scanBarcode(url.data);
        }}
      >
        <>
          <TopWhite title="Отсканируйте QR-код на столике" />
          <View style={styles.qrContainer}>
            <QRScanner width={150} height={150} style={styles.qrScanner} />
          </View>
          <TouchableOpacity
            onPress={() => {
              scanBarcode('https://avemenu.app/code/61ae79f45bbe6784ea0efaac');
            }}
          >
            <Text style={{ color: '#fff', marginBottom: 100 }}>TEST</Text>
          </TouchableOpacity>
        </>
      </RNCamera>
    </Container>
  );

  function renderEmpty() {
    return (
      <View style={styles.text__block}>
        <Text style={styles.text}>Включите камеру </Text>
      </View>
    );
  }

  async function scanBarcode(url) {
    setScanned(true);
    //console.log('URL', url);
    await props.navigation.goBack();
    // Проверить url на формат
    if (!checkUrl(url)) {
      return Alert.alert('Неверный формат QR-кода');
    }

    // const r = await sendQR(url);
    const r = await sendQRcode(url);
    if (!r) {
      Alert.alert('Ошибка сервера');
      return setTimeout(() => setScanned(false), 3000);
    }
    // console.log('CAMERA CAFE', r.data);
    if (r.data.status === 'OK') {
      await props._setCafeInfo(
        r.data.table,
        r.data.cafe,
        url,
        r.data.tableLocation,
      );
      props.navigation.navigate(MENU_SCREEN);
    } else {
      Alert.alert(r.data.message);
      return setTimeout(() => setScanned(false), 3000);
    }
  }
}

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  qrContainer: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    justifyContent: 'center',
  },
  qrScanner: {
    alignSelf: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  text: {
    ...FONTS.btnCaption.h4_16px_dark,
    alignSelf: 'center',
  },
  text__block: {
    justifyContent: 'center',
    height: '100%',
  },
});

const mapDispatchToProps = {
  _setCafeInfo,
};

export default connect(null, mapDispatchToProps)(CameraScreen);
