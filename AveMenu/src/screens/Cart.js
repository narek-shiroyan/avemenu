import React, { useEffect, useState } from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { _clearCart, _deleteCartItem, _setCart } from '../actions/cart';
import CircleBtn from '../components/buttons/CircleBtn';
import CoralButton from '../components/buttons/CoralButton';
import Counter from '../components/panel/Counter';
import { COLORS } from '../constants/colors';
import { CURRENCY } from '../constants/currency';
import { FONTS } from '../constants/fonts';
import { DEVICE_HEIGHT } from '../constants/size';
import Container from '../layouts/Container';
import { createOrder } from '../networks/main';
import { getImage } from '../utils/images';
import { getOrderStorage, setOrderStorage } from '../utils/storage';

function CartScreen(props) {
  const { cart, tableLocation } = props;
  const [totalPrice, setToralPrice] = useState(0);
  // console.log('props.tableLocation', tableLocation);
  useEffect(() => {
    if (!Object.keys(cart).length) {
      return;
    }

    let priceTotal = 0;
    for (const key in cart) {
      if (Object.hasOwnProperty.call(cart, key)) {
        const element = cart[key];
        let t = element.counter * element.price;
        priceTotal += t;
      }
    }
    setToralPrice(priceTotal);
  }, [cart]);

  if (!Object.keys(cart).length) {
    return renderEmpty();
  }

  return (
    <Container>
      <View style={styles.container}>
        <View style={styles.title__container}>
          <Text style={styles.title}>Моя корзина</Text>
        </View>
        <ScrollView style={styles.scrollview}>{renderOrderItem()}</ScrollView>
        <View style={styles.total__container}>
          <View style={styles.total__text__container}>
            <Text style={styles.total__itog}>Итого:</Text>
            <Text style={styles.total__price}>
              {totalPrice} {CURRENCY.ru}
            </Text>
          </View>
          <View style={{ marginTop: 10 }}>
            <CoralButton name={'ОФОРМИТЬ ЗАКАЗ'} cb={sendOrder} />
          </View>
        </View>
      </View>
    </Container>
  );

  function renderOrderItem() {
    let arr = Object.values(cart);
    return arr.map((el, key) => (
      <View key={key} style={styles.item__container}>
        <View style={styles.image__container}>
          {el.images.length ? (
            <Image
              source={{ uri: getImage(el.images[0], 250, 200) }}
              resizeMethod="auto"
              resizeMode="cover"
              style={styles.image}
            />
          ) : (
            <View style={styles.defaultImage} />
          )}
          <CircleBtn
            iconName={'DeleteBtn'}
            cb={() => {
              props._deleteCartItem(el);
            }}
            customStyles={styles.deleteBtn}
          />
          <View style={styles.price__container}>
            <Text style={styles.price__text}>
              {el.price}
              {CURRENCY.ru}
            </Text>
          </View>
        </View>
        <View style={styles.text__container}>
          <Text style={styles.title__text}>{el.name}</Text>
          <Text style={styles.portion__text}>{el.portion}</Text>
          <Counter
            counter={el.counter}
            customStyles={{ borderColor: COLORS.lightPurple2 }}
            customNumberStyle={{ color: COLORS.darkPurpleText }}
            purple={true}
            clickMinus={() => {
              //  проверим чтоб не было 0
              if (el.counter > 1) {
                el.counter = el.counter - 1;
                addToCard(el);
              }
            }}
            clickPlus={() => {
              el.counter = el.counter + 1;
              addToCard(el);
            }}
          />
        </View>
      </View>
    ));
  }

  function addToCard(item) {
    props._setCart(item);
  }

  async function sendOrder() {
    //  отправить заказ на сервер
    let arr = Object.values(cart);
    let d_date = Date.now();
    const { data } = await createOrder(
      arr,
      totalPrice,
      props.table,
      props.cafe._id,
      d_date,
      tableLocation,
    );

    // Проверить ответ сервера
    if (!data || data.status === 'ERROR') {
      return alert(data ? data.message : 'Ошибка сервера');
    }

    const orderId = data.orderId;
    const storageOrder = await getOrderStorage();
    if (!storageOrder) {
      await setOrderStorage({
        url: props.url,
        table: props.table,
        orders: [orderId],
      });
    } else {
      let json = JSON.parse(storageOrder);
      //console.log(json.orders);
      //console.log('storageorder', storageOrder['orders']);
      let newOrders;
      if (!json.orders.includes(orderId)) {
        newOrders = [...json.orders, orderId];
      }

      await setOrderStorage({
        url: props.url,
        table: props.table,
        orders: newOrders,
        tableLocation: tableLocation,
      });
    }

    await props._clearCart();
    props.navigation.goBack();
    // Сохранить в локал стор весь заказ
    // Формат такой, orderId, url,
    // ? очистить редюсер от заказа
  }

  function renderEmpty() {
    return (
      <Container>
        <View style={styles.emptyContainer}>
          <Image
            source={require('../assets/empty2.png')}
            style={styles.empty__image}
          />
          <Text style={styles.empty__main_text}>Ваша корзина пуста</Text>
          <Text style={styles.empty__subtext}>
            Сначала необходимо выбрать блюдо
          </Text>
          <View style={styles.coralBtn}>
            <CoralButton
              name="ВЕРНУТЬСЯ В МЕНЮ"
              cb={() => props.navigation.goBack()}
            />
          </View>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
  emptyContainer: {
    paddingHorizontal: 20,
    marginTop: 100,
    //justifyContent: 'center',
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  defaultImage: {
    width: '100%',
    height: '100%',
    borderRadius: 16,
    // borderRadius: 16,
    backgroundColor: COLORS.lightPurple2,
  },
  empty__main_text: {
    ...FONTS.btnCaption.h2_24px_dark,
    marginTop: 20,
  },
  empty__subtext: {
    ...FONTS.p_16px_dark_center,
    marginTop: 10,
  },
  empty__image: {
    height: DEVICE_HEIGHT / 3,
    resizeMode: 'contain',
    //backgroundColor: '#f2f',
  },
  coralBtn: {
    marginTop: 20,
    width: '100%',
  },
  deleteBtn: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
  scrollview: {
    marginTop: 20,
    height: '70%',
  },
  title__container: {
    marginTop: 25,
  },
  title: {
    ...FONTS.btnCaption.h2_24px_dark,
  },
  total__container: {
    marginTop: 10,
  },
  total__itog: {
    ...FONTS.btnCaption.h3_20px_dark,
  },
  total__price: {
    ...FONTS.btnCaption.total,
  },
  total__text__container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  item__container: {
    width: '100%',
    //height: 250,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginVertical: 5,
    //backgroundColor: '#f2f',
  },
  image__container: {
    width: 163,
    height: 180,
    borderRadius: 16,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 16,
  },
  price__container: {
    width: 50,
    height: 30,
    backgroundColor: COLORS.coral,
    position: 'absolute',
    bottom: 0,
    borderBottomLeftRadius: 16,
    borderTopRightRadius: 16,
    justifyContent: 'center',
  },
  price__text: {
    ...FONTS.secondary_14px_price_old,
    alignSelf: 'center',
  },
  title__text: {
    ...FONTS.btnCaption.h4_16px_dark,
  },
  portion__text: {
    ...FONTS.secondary_13px_text_passive,
  },
  text__container: {
    height: 60,
    width: '45%',
    marginTop: 30,
    marginLeft: 20,
  },
});

const mapStateToProps = state => ({
  cart: state.cart.items,
  cafe: state.user.cafe,
  table: state.user.table,
  tableLocation: state.user.tableLocation,
  url: state.user.url,
});

const mapDispatchToProps = {
  _setCart,
  _deleteCartItem,
  _clearCart,
};

export default connect(mapStateToProps, mapDispatchToProps)(CartScreen);
