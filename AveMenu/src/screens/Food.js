import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { _setCart } from '../actions/cart';
import IconCustom38 from '../components/icons/38x38';
import BottomPrice from '../components/panel/BottomPrice';
import { COLORS } from '../constants/colors';
import { FONTS } from '../constants/fonts';
import { DEVICE_HEIGHT } from '../constants/size';
import Container from '../layouts/Container';
import { getImage } from '../utils/images';

function FoodScreen(props) {
  const { food } = props.route.params;
  console.log(food.images[0]);
  return (
    <Container>
      {food.images.length ? (
        <Image
          source={{ uri: getImage(food.images[0], 500, 450) }}
          style={styles.image}
          resizeMode="cover"
          resizeMethod="scale"
        />
      ) : (
        <View style={styles.defaultImage} />
      )}

      <View style={styles.block}>
        <Text style={styles.title}>{food.name}</Text>
        <View style={styles.portion__block}>
          <IconCustom38 name="Dish" size={38} style={{ alignSelf: 'center' }} />
          <Text style={styles.text__portion}>{food.portion}</Text>
        </View>

        {/* Описание */}
        <View style={{ marginTop: 20 }}>
          <Text style={styles.about__title}>Описание</Text>
          <Text style={styles.about__description}>{food.description}</Text>
        </View>
      </View>
      <BottomPrice food={food} cb={addToCard} />
    </Container>
  );

  function addToCard(item) {
    props.navigation.goBack();
    props._setCart(item);
  }
}

const styles = StyleSheet.create({
  image: {
    height: DEVICE_HEIGHT / 2.5,
    width: '100%',
  },
  defaultImage: {
    height: DEVICE_HEIGHT / 2.5,
    width: '100%',
    // borderRadius: 16,
    backgroundColor: COLORS.lightPurple2,
  },
  block: {
    width: '100%',
    height: '100%',
    borderTopRightRadius: 24,
    borderTopLeftRadius: 24,
    marginTop: -20,
    backgroundColor: COLORS.white,
    paddingTop: 30,
    paddingHorizontal: 15,
  },
  backBottom: {
    position: 'absolute',
    top: 10,
    left: 10,
    zIndex: 20,
  },
  portion__block: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'flex-start',
    height: 50,
    marginTop: 20,
  },
  title: {
    ...FONTS.btnCaption.h3_20px_dark,
  },
  text__portion: {
    ...FONTS.secondary_13px_text,
    alignSelf: 'center',
    marginLeft: 5,
  },

  about__title: {
    ...FONTS.btnCaption.h4_16px_dark,
  },
  about__description: {
    ...FONTS.p_14px_text,
    marginTop: 10,
  },
});

const mapDispatchToProps = {
  _setCart,
};

export default connect(null, mapDispatchToProps)(FoodScreen);
