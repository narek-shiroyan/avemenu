import { useIsFocused } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { Alert, Platform, StyleSheet, View, Linking } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { connect } from 'react-redux';
import { _clearOrdersItem } from '../actions/orders';
import { _setCafeInfo } from '../actions/user';
import IconButton from '../components/buttons/IconButton';
import NavigateButton from '../components/buttons/NavigateButton';
import QRButton from '../components/buttons/QRButton';
import CurrentCafe from '../components/carousel/CurrentCafe';
import { CafeModal } from '../components/modal/CafeModal';
import TopWhite from '../components/panel/TopWhite';
import { COLORS } from '../constants/colors';
import { FONTS } from '../constants/fonts';
import {
  CAMERA_SCREEN,
  MENU_SCREEN,
  PROFILE_SCREEN,
} from '../constants/navigations';
import { DEVICE_HEIGHT, DEVICE_WIDTH } from '../constants/size';
import Container from '../layouts/Container';
import {
  getAllCafes,
  getOrderStatus,
  sendQR,
  sendQRcode,
} from '../networks/main';
import { getCurrentPosition } from '../utils/geolocation.ios';
import { checkUrl } from '../utils/regex';
import {
  cleanOrderStorage,
  getOrderStorage,
  setOrderStorage,
} from '../utils/storage';

let DEBUG = false;
const latDELTA = 0.28;
const lonDELTA = latDELTA * (DEVICE_WIDTH / DEVICE_HEIGHT);

function MainMapScreen(props) {
  const _map = React.useRef(null);
  const cafeModalRef = React.useRef(null);
  //const { scan } = props.navigation.params;
  const isFocused = useIsFocused();
  const [markers, setMarkers] = useState([]);
  const [pickMarker, setPickMarker] = useState({}); //Если пикнул, то включается
  const [showCurrentCafe, setShowCurrentCafe] = useState(false); // Показать текущее заведение
  const [currentCafe, setCurrentCafe] = useState(null); // Информация по сохраненной кафешке
  const [table, setTable] = useState(null);
  const [url, setUrl] = useState(null);

  const [coordinates, setCoordinates] = useState({
    latitude: 0,
    longitude: 0,
  });

  const [loading, setLoading] = useState(true); // Общая загрузка

  /** ---------------USE EFFECT ------------------- */
  useEffect(() => {
    const listener = Linking.addEventListener('url', handleOpenURL);
    if (isFocused) {
      loadCoordinates();
      getData();
      loadStorage();
    }
    return () => {
      listener.remove();
    };
  }, [isFocused]);
  /** ------------------- END USE EFFECT ------------------- */

  const onOpen = () => {
    cafeModalRef.current?.open();
  };
  const onClose = () => {
    cafeModalRef.current?.close();
  };

  // if (loading) {
  //     return <Text>Загрузка</Text>;
  // }

  return (
    <Container>
      <MapView
        ref={_map}
        style={styles.mapContainer}
        provider={PROVIDER_GOOGLE}
        compassOffset={{ x: 50, y: 100 }}
        showsUserLocation
        // showsMyLocationButton
        showsScale
        //mapType={Platform.OS === 'android' ? 'none' : 'standard'}
        zoomEnabled
        // loading={loading}
        zoomControlEnabled
        followsUserLocation
        loadingEnabled={loading}
        initialRegion={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
          latitudeDelta: latDELTA,
          longitudeDelta: lonDELTA,
        }}
      >
        {markers.map((el, key) => (
          <Marker
            key={key}
            coordinate={{
              latitude: el.lat,
              longitude: el.lon,
            }}
            onPress={async e => {
              setPickMarker(el);
              await _map.current.animateToRegion(
                {
                  latitude: el.lat,
                  longitude: el.lon,
                  latitudeDelta: latDELTA,
                  longitudeDelta: lonDELTA,
                },
                500,
              );
              onOpen();
            }}
          />
        ))}
      </MapView>

      <TopWhite title="Заведения" />

      <NavigateButton cb={onLocationIconPress} />

      {!showCurrentCafe ? (
        <>
          <QRButton cb={() => props.navigation.navigate(CAMERA_SCREEN)} />

          <View style={styles.cornerButtons}>
            <IconButton
              name="User"
              cb={() => props.navigation.navigate(PROFILE_SCREEN)}
            />
            {/* <IconButton
                            name="Burger"
                            cb={() => props.navigation.navigate(MENU_SCREEN)}
                        /> */}
          </View>
        </>
      ) : (
        <CurrentCafe
          cafe={currentCafe}
          cb={async () => {
            await props._setCafeInfo(table, currentCafe, url);
            props.navigation.navigate(MENU_SCREEN);
          }}
        />
      )}

      {/* Modal Cafe */}
      <CafeModal ref={cafeModalRef} {...pickMarker} />
    </Container>
  );

  async function getData() {
    try {
      const req = await getAllCafes();
      if (!req) {
        return setLoading(false);
      }
      if (req.data.status === 'OK') {
        setMarkers(req.data.items);
        setLoading(false);
      } else {
        setLoading(false);
        alert('Ошибка');
      }
    } catch (e) {
      console.log(e);
      setLoading(false);
    }
  }

  async function loadStorage() {
    // Тянем данные
    const items = await getOrderStorage();
    if (!items) {
      await cleanOrderStorage();
      await props._clearOrdersItem();
      return setShowCurrentCafe(false);
    }

    let json = JSON.parse(items);
    // если все же что-то есть
    if (json.url && json.orders.length !== 0) {
      // Проверка статусов
      for (let i = 0; i < json.orders.length; i++) {
        const element = json.orders[i];
        // отправка id для проверки статуса заказа
        const { data } = await getOrderStatus(element);
        if (!data || data.status !== 'OK') {
          return;
        }

        // Проверить есть, ли законченные статусы
        if (data.message.status === 'end' || data.message.status === 'cancel') {
          // Удалить данный заказ из storage
          console.log('на удаление', element);
          delete json.orders.splice(i, 1);
          //removedOrders.push(element);
        }
      }
    }

    // Обновить storage с активными заказами
    //console.log('JSON', json);

    // Теперь проверить, сколько активных заказов осталось
    // Если их 0, то очистить storage
    //console.log('JSON URL', json.url);
    if (!json.orders.length) {
      await props._clearOrdersItem();
      setShowCurrentCafe(false);
      return await cleanOrderStorage();
    } else {
      setOrderStorage(json);

      // Проверить qr
      if (!checkUrl(json.url)) {
        return;
      }
      //const r = await sendQR(json.url);
      const r = await sendQRcode(json.url);
      // console.log('R DATA', r.data);
      if (r.data.status === 'OK') {
        // console.log('rrr', r.data.tableLocation);
        await props._setCafeInfo(
          r.data.table,
          r.data.cafe,
          json.url,
          r.data.tableLocation,
        );
        setTable(r.data.table);
        setUrl(json.url);
        setCurrentCafe(r.data.cafe);
        setShowCurrentCafe(true);
        // console.log('R CAFE', r.data.cafe);
        //props.navigation.navigate(ORDERS_SCREEN);
      } else {
        return;
      }
    }
  }

  function onLocationIconPress() {
    const loc = {
      latitude: coordinates.latitude,
      longitude: coordinates.longitude,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    };
    _map.current.animateToRegion(loc, 500);
  }

  // function getLinking() {
  //     if (Platform.OS === 'android') {
  //         Linking.getInitialURL().then(url => {
  //             navigateLink(url);
  //         });
  //     } else {
  //         Linking.addEventListener('url', handleOpenURL);
  //     }
  // }

  function navigateLink(url) {
    const route = url.replace(/.*?:\/\//g, '');
    const code = route.match(/\/([^\/]+)\/?$/)[1];
    // const routeName = route.split('/')[0];
    if (!code) {
      return;
    }

    getCodeByDeepLink(code);

    // if (routeName === 'people') {
    //     navigate('People', { id, name: 'chris' });
    // }
  }

  async function getCodeByDeepLink(code) {
    const items = await getOrderStorage();
    console.log('items', items);
    if (!items) {
      const r = await sendQRcode(`https://avemenu.app/code/${code}`);
      if (!r) {
        return Alert.alert('Ошибка сервера');
      }
      // console.log('CAMERA CAFE', r.data);
      if (r.data.status === 'OK') {
        await props._setCafeInfo(
          r.data.table,
          r.data.cafe,
          `https://avemenu.app/code/${code}`,
          r.data.tableLocation,
        );
        props.navigation.navigate(MENU_SCREEN);
      } else {
        return Alert.alert(r.data.message);
      }
    }
  }

  function handleOpenURL(event) {
    navigateLink(event.url);
  }

  async function loadCoordinates() {
    setLoading(true);
    if (DEBUG) {
      setLoading(false);
      setCoordinates({
        latitude: 55.403124,
        longitude: 36.611693,
      });

      return;
    }
    const r = await getCurrentPosition();

    if (!r) {
      setLoading(false);
      const loc = {
        latitude: 55.7522,
        longitude: 37.6156,
        latitudeDelta: 0.1922,
        longitudeDelta: 0.1421,
      };
      _map.current.animateToRegion(loc, 500);
      return Alert.alert('Включите геолокацию');
    }

    setCoordinates({
      latitude: r.latitude,
      longitude: r.longitude,
    });

    const loc = {
      latitude: r.latitude,
      longitude: r.longitude,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    };
    _map.current.animateToRegion(loc, 500);
    setLoading(false);
  }
}

const styles = StyleSheet.create({
  test: {
    ...FONTS.btnCaption.h1_30px_dark,
  },
  qrButtonContainer: {
    position: 'absolute',
    bottom: 100,
    alignSelf: 'center',
  },
  cornerButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '85%',
    position: 'absolute',
    bottom: Platform.OS === 'android' ? 40 : 80,
    alignSelf: 'center',
  },
  mapContainer: {
    width: DEVICE_WIDTH,
    height: DEVICE_HEIGHT,
    ...StyleSheet.absoluteFillObject,
  },
  circle: {
    width: 26,
    height: 26,
    borderRadius: 50,
  },
  stroke: {
    backgroundColor: COLORS.white,
    borderRadius: 50,
    width: '100%',
    height: '100%',
    zIndex: 1,
  },
  core: {
    backgroundColor: COLORS.coral,
    width: 24,
    position: 'absolute',
    top: 1,
    left: 1,
    right: 1,
    bottom: 1,
    height: 24,
    borderRadius: 50,
    zIndex: 2,
  },
});

const mapDispatchToProps = {
  _setCafeInfo,
  _clearOrdersItem,
};

export default connect(null, mapDispatchToProps)(MainMapScreen);
