import { useIsFocused } from '@react-navigation/native';
import _ from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import SectionList from 'react-native-tabs-section-list';
import { connect } from 'react-redux';
import { _setOrder } from '../actions/orders';
import CarouselCafe from '../components/carousel/carousel';
import FoodCart from '../components/cart/FoodCart';
import BottomCardPanel from '../components/panel/BottomCardPanel';
import { COLORS } from '../constants/colors';
import { FONTS } from '../constants/fonts';
import { CART_SCREEN, FOOD_SCREEN } from '../constants/navigations';
import { DEVICE_HEIGHT, DEVICE_WIDTH } from '../constants/size';
import Container from '../layouts/Container';
import { getOrderStorage } from '../utils/storage';

function MenuScreen(props) {
  //const cafe = markers[1];
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const isFocused = useIsFocused();
  const list = useRef();
  const { cafe, table } = props;
  useEffect(() => {
    if (isFocused) {
      dataMaker();
      getOrders();
    }
  }, [isFocused]);

  if (loading) {
    return <Text>Загрузка</Text>;
  }

  return (
    <Container>
      <SectionList
        sections={items}
        keyExtractor={(item, index) => `${item._id}-${index}`}
        //stickySectionHeadersEnabled={true}
        scrollToLocationOffset={0}
        scrollsToTop
        tabBarStyle={styles.tabBar}
        initialNumToRender={100}
        nestedScrollEnabled={true}
        contentContainerStyle={styles.sectionStyle}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={() => (
          <View style={styles.header__block}>
            <View style={styles.header__title}>
              <Text style={styles.cafe__title}>{cafe.name}</Text>
            </View>
            <CarouselCafe images={cafe.images ? cafe.images : []} />
          </View>
          // <View style={styles.header__block}>
          //     <Image
          //         source={{ uri: cafe.images[0] }}
          //         style={styles.header__image}
          //         resizeMode={'cover'}
          //     />
          // </View>
        )}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
        renderTab={({ title, isActive }) => (
          <View
            style={[
              styles.tabContainer,
              isActive ? styles.tabContainer__active : null,
            ]}
          >
            <Text
              style={[styles.tabText, isActive ? styles.tabText__active : null]}
            >
              {title}
            </Text>
          </View>
        )}
        renderSectionHeader={({ section }) => (
          <View style={{ marginBottom: 50 }}>
            <View style={styles.sectionHeaderContainer} />
            <Text style={styles.sectionHeaderText}>{section.title}</Text>
          </View>
        )}
        renderItem={r => {
          function click() {
            props.navigation.navigate(FOOD_SCREEN, {
              food: r.item,
            });
          }
          return (
            <View style={styles.itemContainer} key={r.item._id}>
              {/* <View style={styles.itemRow}>
                            <Text style={styles.itemTitle}>{item.title}</Text>
                            <Text style={styles.itemPrice}>${item.price}</Text>
                        </View>
                        <Text style={styles.itemDescription}>
                            {item.description}
                        </Text> */}
              <FoodCart {...r.item} click={click} />
            </View>
          );
        }}
      />
      {Object.keys(props.cart).length ? renderCartPanel() : null}
    </Container>
  );

  async function dataMaker() {
    let newItem = [];
    const d = _.groupBy(cafe.menu, function (b) {
      return b.group;
    });

    // delete d['Хлеб'];
    // delete d['Выпечка'];
    // delete d['Напитки'];
    // delete d['Гарниры'];
    // delete d['Холодные закуски}'];
    for (const key in d) {
      if (Object.hasOwnProperty.call(d, key)) {
        const element = d[key];
        newItem.push({
          title: key,
          data: element,
        });
      }
    }
    setItems(newItem);
    setLoading(false);
  }

  async function getOrders() {
    const data = await getOrderStorage();
    if (data) {
      let newData = JSON.parse(data);
      props._setOrder(newData.orders);
    }
  }

  function renderCartPanel() {
    return (
      <BottomCardPanel
        count={Object.keys(props.cart).length}
        cb={() => props.navigation.navigate(CART_SCREEN)}
      />
    );
  }
}

const styles = StyleSheet.create({
  cafe__title: {
    //...FONTS.btnCaption.h2_24px_dark,
    ...FONTS.btnCaption.montSerat,
  },
  tabBar: {
    backgroundColor: '#fff',
    borderBottomColor: '#f4f4f4',
    borderBottomWidth: 1,
    //marginTop: 100,
  },
  header__block: {
    justifyContent: 'center',
    //height: DEVICE_HEIGHT / 3,
    width: DEVICE_WIDTH,
    marginTop: 10,
    //paddingHorizontal: 10,
  },
  header__title: {
    //position: 'absolute',
    marginVertical: 10,
    //paddingLeft: 10,
    alignSelf: 'center',
    //alignSelf: 'flex-start',
    //justifyContent: 'center',
  },
  header__image: {
    height: '100%',
    width: '100%',
  },
  sectionStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    marginBottom: 150,
    width: DEVICE_WIDTH,
    paddingBottom: 150,
  },
  tabContainer: {
    //height: 35,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderColor: COLORS.lightPurple2,
    borderWidth: 3,
    borderRadius: 8,
    //marginHorizontal: 3,
    marginLeft: 10,
    marginBottom: 10,
    marginTop: 10,
    flexDirection: 'row',
  },
  tabContainer__active: {
    backgroundColor: COLORS.purple,
    borderColor: COLORS.purple,
  },
  tabText: {
    ...FONTS.secondary_13px_tag_normal,
  },
  tabText__active: {
    color: COLORS.white,
  },
  separator: {
    height: 0.5,
    width: '100%',
    alignSelf: 'flex-end',
    backgroundColor: '#eaeaea',
  },
  sectionHeaderContainer: {
    height: 10,
    backgroundColor: '#f6f6f6',
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1,
    borderBottomColor: '#f4f4f4',
    borderBottomWidth: 1,
    width: DEVICE_HEIGHT,
  },
  sectionHeaderText: {
    ...FONTS.btnCaption.h2_24px_dark,
    // color: '#010101',
    backgroundColor: COLORS.white,
    // fontSize: 23,
    // fontWeight: 'bold',
    paddingTop: 10,
    paddingBottom: 10,
    paddingHorizontal: 15,
    width: '100%',
  },
  itemContainer: {
    paddingVertical: 20,
    paddingHorizontal: 10,
    backgroundColor: COLORS.white,
    width: DEVICE_WIDTH / 2,
    alignItems: 'flex-start',
  },
  foodIcon: {
    marginRight: 5,
  },
});

const mapStateToProps = state => ({
  cart: state.cart.items,
  cafe: state.user.cafe,
  table: state.user.table,
});

const mapDispatchToProps = {
  _setOrder,
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuScreen);
