import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import CancelIcon from '../assets/icons/16x16/cancelled.svg';
import WaitIcon from '../assets/icons/16x16/clock.svg';
import SuccessIcon from '../assets/icons/16x16/done.svg';
import EndIcon from '../assets/icons/16x16/end.svg';
import CoralButton from '../components/buttons/CoralButton';
import PressButton from '../components/buttons/PressButton';
import { COLORS } from '../constants/colors';
import { CURRENCY } from '../constants/currency';
import { FONTS } from '../constants/fonts';
import { DEVICE_HEIGHT } from '../constants/size';
import Container from '../layouts/Container';
import { changeOrderStatus, getOrdersByArray } from '../networks/main';
import { getImage } from '../utils/images';
import { deleteOrderStorage, getOrderStorage } from '../utils/storage';

export default function OrdersScreen(props) {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    loadData();
  }, []);

  if (loading) {
    return (
      <Container>
        <View style={styles.loading__container}>
          <ActivityIndicator />
        </View>
      </Container>
    );
  }

  if (!items.length) {
    return renderEmpty();
  }
  return (
    <Container>
      <View style={styles.container}>
        <View style={styles.title__container}>
          <Text style={styles.title}>Мои заказы</Text>
        </View>

        <FlatList
          data={items}
          style={styles.scrollview}
          renderItem={orderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </Container>
  );

  function orderItem({ item }) {
    let date = new Date(Number(item.date));
    let time = {
      day: date.getDate(),
      month: date.getMonth() + 1,
      year: date.getFullYear(),
      hour: date.getHours(),
      minutes: date.getMinutes(),
    };

    return (
      <View style={styles.order__container}>
        <Text style={styles.order__text}>Заказ</Text>

        <View style={styles.pay__text}>
          {renderOrderStatus(item.status)}
          <Text
            style={{
              ...FONTS.secondary_13px_text_passive,
              marginLeft: 5,
            }}
          >
            / {time.day}.{time.month}.{time.year}, {time.hour}:{time.minutes}
          </Text>
        </View>

        {item.items.map((el, key) => (
          <View key={key} style={styles.item__container}>
            <View style={styles.image__container}>
              {el.images.length ? (
                <Image
                  source={{
                    uri: getImage(el.images[0], 250, 200),
                  }}
                  resizeMethod="auto"
                  resizeMode="cover"
                  style={styles.image}
                />
              ) : (
                <View style={styles.deafultImage} />
              )}
            </View>
            <View style={styles.text__container}>
              <Text style={styles.title__text}>{el.name}</Text>
              <Text style={styles.portion__text}>{el.portion}</Text>
              <View style={{ marginTop: 10 }}>
                <Text style={{ ...FONTS.p_14px_text }}>
                  {el.counter} x{' '}
                  <Text
                    style={{
                      ...FONTS.secondary_14px_price_cart,
                    }}
                  >
                    {' '}
                    {el.price} {CURRENCY.ru}
                  </Text>
                </Text>
              </View>
            </View>
          </View>
        ))}
        {item.status === 'wait' ? (
          <PressButton
            title={'Отменить заказ'}
            cb={async () => {
              await changeOrderStatus(item._id, 'cancel');
              await deleteOrderStorage(item._id);
              await loadData();
            }}
          />
        ) : null}
      </View>
    );
  }

  async function loadData() {
    const json = await getOrderStorage();
    if (!json) {
      setItems([]);
      return setLoading(false);
    }
    const jsonItems = JSON.parse(json);
    if (!jsonItems.orders.length || !jsonItems) {
      setItems([]);
      return setLoading(false);
    }

    try {
      const { data } = await getOrdersByArray(jsonItems.orders);
      setItems(data.items);
      setLoading(false);
    } catch (e) {
      console.log(e);
      setItems([]);
      setLoading(false);
    }
  }

  function renderOrderStatus(s) {
    if (s === 'wait') {
      return (
        <>
          <WaitIcon size={16} />
          <Text style={styles.order__status__desctiption}>
            Ожидает подтверждения
          </Text>
        </>
      );
    }
    if (s === 'end') {
      return (
        <>
          <EndIcon size={16} />
          <Text style={styles.order__status__desctiption}>Заказ завершен</Text>
        </>
      );
    }
    if (s === 'accept') {
      return (
        <>
          <SuccessIcon size={16} />
          <Text style={styles.order__status__desctiption}>Заказ принят</Text>
        </>
      );
    }

    if (s === 'cancel') {
      return (
        <>
          <CancelIcon size={16} />
          <Text style={styles.order__status__desctiption}>Заказ отменен</Text>
        </>
      );
    }
  }

  function renderEmpty() {
    return (
      <Container>
        <View style={styles.emptyContainer}>
          <Image
            source={require('../assets/empty_orders.png')}
            style={styles.empty__image}
          />
          <Text style={styles.empty__main_text}>
            У вас нет активных заказов
          </Text>
          <Text style={styles.empty__subtext}>
            Сначала необходимо сделать заказ
          </Text>
          <View style={styles.coralBtn}>
            <CoralButton
              name="ВЕРНУТЬСЯ В МЕНЮ"
              cb={() => props.navigation.goBack()}
            />
          </View>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
  },
  order__container: {
    //borderColor: COLORS.lightPurple2,
    //borderWidth: 3,
    borderRadius: 8,
    paddingHorizontal: 15,
    marginHorizontal: 10,
    marginVertical: 20,
    backgroundColor: COLORS.white,
    shadowColor: COLORS.coral,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  pay__text: {
    ...FONTS.secondary_13px_text,
    marginTop: 10,
    marginBottom: 20,
    flexDirection: 'row',
  },
  order__text: {
    ...FONTS.links.link_16px_normal,
    marginTop: 20,
  },
  loading__container: {
    justifyContent: 'center',
    height: DEVICE_HEIGHT,
  },
  title__container: {
    marginTop: 25,
  },
  scrollview: {
    marginTop: 20,
    height: DEVICE_HEIGHT / 1.2,
  },
  title: {
    ...FONTS.btnCaption.h2_24px_dark,
  },
  emptyContainer: {
    paddingHorizontal: 20,
    marginTop: 100,
    //justifyContent: 'center',
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },

  empty__main_text: {
    ...FONTS.btnCaption.h2_24px_dark,
    marginTop: 20,
  },
  empty__subtext: {
    ...FONTS.p_16px_dark_center,
    marginTop: 10,
  },
  empty__image: {
    height: DEVICE_HEIGHT / 3,
    resizeMode: 'contain',
    //backgroundColor: '#f2f',
  },
  coralBtn: {
    marginTop: 20,
    width: '100%',
  },
  item__container: {
    width: '100%',
    //height: 250,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginVertical: 5,
  },
  image__container: {
    width: 123,
    height: 140,
    borderRadius: 16,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 16,
  },
  deafultImage: {
    width: '100%',
    height: '100%',
    borderRadius: 16,
    backgroundColor: COLORS.lightPurple2,
  },
  order__status__desctiption: {
    marginLeft: 5,
  },
  price__container: {
    width: 50,
    height: 30,
    backgroundColor: COLORS.coral,
    position: 'absolute',
    bottom: 0,
    borderBottomLeftRadius: 16,
    borderTopRightRadius: 16,
    justifyContent: 'center',
  },
  price__text: {
    ...FONTS.secondary_14px_price_old,
    alignSelf: 'center',
  },
  title__text: {
    ...FONTS.btnCaption.h4_16px_dark,
  },
  portion__text: {
    ...FONTS.secondary_13px_text_passive,
    marginTop: 10,
  },
  text__container: {
    height: 60,
    width: '60%',
    marginTop: 30,
    marginLeft: 20,
  },
});
