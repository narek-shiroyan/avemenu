import React from 'react';
import { StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview';

export default function PolicyScreen() {
  return <WebView source={{ uri: 'https://avemenu.app/privacy' }} />;
}

const styles = StyleSheet.create({});
