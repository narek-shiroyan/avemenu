import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { COLORS } from '../constants/colors';
import { FONTS } from '../constants/fonts';
import {
  ABOUT_SCREEN,
  POLICY_SCREEN,
  TERMS_SCREEN,
} from '../constants/navigations';
import { DEVICE_HEIGHT } from '../constants/size';
import Container from '../layouts/Container';

export default function ProfileScreen(props) {
  const { navigation } = props;
  return (
    <Container>
      <ScrollView>
        <Image
          source={require('../assets/Illustration.png')}
          style={styles.image}
          resizeMethod={'auto'}
          resizeMode={'contain'}
        />
        <View style={styles.info__container}>
          <Text style={styles.text__main}>Меню под рукой</Text>
          <Text style={styles.text__info}>
            Заказать блюдо стало еще удобнее. Отсканируйте QR-код на столике в
            заведении и получите меню.{' '}
          </Text>
        </View>

        <View style={styles.button__group__container}>
          <Text style={styles.button__text__title}>Информация</Text>
          <TouchableOpacity
            style={styles.button__container}
            onPress={() => navigation.navigate(ABOUT_SCREEN)}
          >
            <Text style={styles.button__text}>О приложении</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button__container}
            onPress={() => navigation.navigate(POLICY_SCREEN)}
          >
            <Text style={styles.button__text}>Политика конфиденциальности</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button__container}
            onPress={() => navigation.navigate(TERMS_SCREEN)}
          >
            <Text style={styles.button__text}>Условия использования</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Container>
  );
}

const styles = StyleSheet.create({
  info__container: {
    alignItems: 'center',
  },
  text__info: {
    ...FONTS.p_16px_dark_center,
    marginTop: 10,
    paddingHorizontal: 10,
  },
  text__main: {
    marginTop: 20,
    ...FONTS.btnCaption.h2_24px_dark,
  },
  button__group__container: {
    marginTop: 30,
  },
  button__text__title: {
    ...FONTS.btnCaption.h3_20px_dark,
    marginBottom: 20,
    paddingLeft: 20,
    borderBottomColor: COLORS.lightBlue,
    borderBottomWidth: 2,
  },
  button__container: {
    borderBottomWidth: 1,
    borderBottomColor: COLORS.lightBlue,
    paddingVertical: 20,
    paddingHorizontal: 20,
  },
  button__text: {
    ...FONTS.links.link_16px_normal,
  },
  image: {
    //width: '100%',
    height: DEVICE_HEIGHT / 3.5,
    alignSelf: 'center',
  },
});
