import React from 'react';
import { StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview';

export default function TermsScreen() {
  return <WebView source={{ uri: 'https://avemenu.app/privacy' }} />;
}

const styles = StyleSheet.create({});
