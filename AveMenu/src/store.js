import { combineReducers } from 'redux';
import user from './reducers/user';
import orders from './reducers/orders';
import cart from './reducers/cart';

export default combineReducers({
  user,
  orders,
  cart,
});