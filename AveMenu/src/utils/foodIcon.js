export const FOOD_NAME = {
  'Блюда на огне': 'Meat',
  'Первые блюда': 'HotMeals',
  'Вторые блюда': 'MainCourse',
  'Салаты': 'Salad',
  'Горячие салаты': 'WarnSalad',
  'Гарниры': 'Garnik',
  'Холодные закуски': 'Snacks',
  'Соусы': 'Sause',
  'Выпечка': 'Bacery',
  'Напитки': 'Drinks',
  'Избранное': 'FullStar',
  'Хлеб': 'Bacery',
};
