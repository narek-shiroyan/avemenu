import GetLocation from 'react-native-get-location';
import { PermissionsAndroid, Alert } from 'react-native';
/**
 * Функция выдачи текущик координат
 * @return {lat, lon}
 */
export const getCurrentPosition = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      const position = await GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 15000,
      });
      return position;
    } else {
      Alert.alert('Включите доступ к геолокации');
    }
  } catch (e) {
    console.warn(e);
  }
};
