import { HOST } from '../constants/config';

export const getImage = (img, width, height) => {
  return `${HOST}/images/${img}/${width}/${height}`;
};
