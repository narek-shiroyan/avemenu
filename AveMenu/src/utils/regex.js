export const checkUrl = url => {
  let l = new RegExp(/https:\/\/avemenu.app[/]{1}code[/]{1}[a-z0-9]{24}$/);
  return l.test(url);
};
