import AsyncStorage from '@react-native-async-storage/async-storage';

export const setOrderStorage = async value => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem('@orders', jsonValue);
  } catch (e) {
    // saving error
    console.log(e);
  }
};

export const getOrderStorage = async () => {
  try {
    const value = await AsyncStorage.getItem('@orders');
    if (value !== null) {
      // value previously stored
      return value;
    }
  } catch (e) {
    // error reading value
    console.log(e);
  }
};

export const deleteOrderStorage = async id => {
  try {
    const value = await AsyncStorage.getItem('@orders');
    const jsonValue = JSON.parse(value);
    // Теперь удалить id из orders
    // А потом вставить обратно обновленный orders
    for (let i = 0; i < jsonValue.orders.length; i++) {
      if (jsonValue.orders[i] === id) {
        delete jsonValue.orders.splice(i, 1);
      }
    }
    // // Если вдруг это был последний заказ
    // if (!jsonValue.orders.length) {
    //     return await cleanOrderStorage();
    // }
    await AsyncStorage.setItem('@orders', JSON.stringify(jsonValue));
  } catch (e) {
    // saving error
    console.log(e);
  }
}

export const cleanOrderStorage = async () => {
  try {
    await AsyncStorage.removeItem('@orders');
  } catch (e) {
    // remove error
    console.log(e);
  }
};